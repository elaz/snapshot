package basic;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

final class ReportCollector implements Iterable<Report> {
    final Report[] reportTails = new Report[NUM_THREADS];
    final Report[] reportHeads = new Report[NUM_THREADS];
    volatile ThreadLocal<Report> tail = ThreadLocal.withInitial(() -> reportHeads[tid()]);

    ReportCollector() {
	for (int i = 0; i < NUM_THREADS; i++)
	    reportTails[i] = reportHeads[i] = Report.create(null, null);
    }

    void report(Node node, Report.Type t) {
	Report newItem = Report.create(node, t);
	/*
	 * if (reportTails[tid()].next().compareAndSet(null, newItem))
	 * reportTails[tid()] = newItem;
	 */
	if (tail.get().next().compareAndSet(null, newItem))
	    tail.set(newItem);
    }

	
    /**
     * Required to synchronize between multiple iterators.
     * 
     * After this method is completed, any further calls to Report will do
     * nothing. Calls to @report concurrent with @blockFurtherReports may
     * succeed or fail arbitrarily.
     */
    void blockFurtherReports() {
	for (Report tail : reportTails) {
	    if (tail.next().get() == null)
		tail.next().compareAndSet(null, Report.BLOCKER);
	    // If we failed to do this, all is fine as well.
	    // Either:
	    // 1. someone else has blocked
	    // 2. The thread has reported, and will not try to report again
	    // (since this IteratorSaver should be disconnected from the list
	    // prior to calling this method).
	}
    }

    private static int tid() {
	return (int) Thread.currentThread().getId();
    }

    private Set<Report> getAllReports() {
	Set<Report> res = new LinkedHashSet<>();
	for (Report head : reportHeads) {
	    for (Report curr = head.next().get(); curr != Report.BLOCKER; curr = curr.next().get()) {
		res.add(curr);
	    }
	}
	return res;
    }

    @Override
    public Iterator<Report> iterator() {
	return getAllReports().iterator();
    }

    private static final int NUM_THREADS = 42;
}

interface Report {
    AtomicReference<Report> next();

    Node node();

    Type type();

    static Report create(final Node node, final Type type) {
	final AtomicReference<Report> next = new AtomicReference<>(null);
	return new Report() {
	    public AtomicReference<Report> next() { return next; }
	    public Node node() { return node; }
	    public Type type() { return type; }
	};
    }

    enum Type {
	ADD, REMOVE, BLOCK;
    }

    static final Report BLOCKER = create(null, Type.BLOCK);
    static final Report DUMMY = null;
}
