package basic;

import java.util.Collection;
import java.util.Set;
import static basic.Report.Type;
/**
 * Should be called "SnapshotBuilder", but whatever
 */
final class SnapCollector implements ICollector {
    private volatile boolean active = true;
    final NodeCollector nodesCollector = new NodeCollector();
    final ReportCollector reportsCollector = new ReportCollector();

    @Override
    public Collection<?> collectSnapshot(Node head) {
	Node n = head;
	while (n != null) {
	    if (!isActive())
		break;
	    if (!n.isMarked())
		n = nodesCollector.addNode(n);
	    n = n.next().get();
	}
	nodesCollector.blockFurtherNodes();
	this.deactivate();
	reportsCollector.blockFurtherReports();
	return reconstructUsingReports();
    }

    @Override
    public void reportInsert(Node newNode) {
	if (!isActive())
	    return;
	if (newNode.isMarked())
	    return;
	reportsCollector.report(newNode, Type.ADD);
    }

    @Override
    public void reportDelete(Node victim) {
	if (!isActive())
	    return;
	reportsCollector.report(victim, Type.REMOVE);
    }
    
    @Override
    public boolean isActive() {
	return this.active;
    }

    private void deactivate() {
	this.active = false;
    }

    private Collection<?> reconstructUsingReports() {
	Set<Node> res = nodesCollector.getAllNodes();
	reportsCollector.forEach(r -> {
	    Node n = r.node();
	    switch (r.type()) {
	    case ADD: res.add(n); break;
	    case REMOVE: res.remove(n); break;
	    case BLOCK: break;
	    }
	});
	return res;
    }

}
