package basic;

import java.util.Collection;

public interface ICollector {

    Collection<?> collectSnapshot(Node from);

    /**
     * Analogue to reporting a new value in array B.
     *
     * @param newNode to insert
     * @postcondition Installs the INSERT report.
     * May fail to install the report if {@code blockFurtherReports()} has previously been invoked.
     */
    void reportInsert(Node newNode);

    /**
     * Analogue to reporting a new value in array B.
     *
     * @param victim to delete
     * @postcondition Installs the DELETE report.
     * May fail to install the report if {@code blockFurtherReports()} has previously been invoked.
     */
    void reportDelete(Node victim);


    /**
     * Analogue to reading the {@code ongoingScan} binary field. 
     * @return {@code true} if the {@code deactivate()} method has not yet been called, and {@code false} otherwise.
     * <p>(True means the iteration is still ongoing and further pointers might still be installed in the snapshot object.)</p>
     */
    boolean isActive();


    static ICollector create() {
        return new SnapCollector();
    }


    ICollector DEACTIVATED = new ICollector() {
        public final void reportInsert(Node newNode) {
        }

        public final void reportDelete(Node victim) {
        }

        public final Collection<?> collectSnapshot(Node n) {
            return null;
        }

        public final boolean isActive() { return false;	}
    };
}