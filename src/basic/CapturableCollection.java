package basic;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;

import static basic.Utils.onDelete;
import static basic.Utils.onExistence;
import static basic.Utils.onBetweenMarkAndRemove;

public final class CapturableCollection {
    private ILockFreeOrderedSet collection;

    public CapturableCollection(ILockFreeOrderedSet collection) {
	this.collection = collection;
    }

    public boolean delete(int key) {
	ICollector sc = getSC();
	return this.collection.delete(key, onBetweenMarkAndRemove = sc::reportDelete, onDelete = sc::reportDelete);
    }

    public boolean insert(Node n) {
	ICollector sc = getSC();
	return this.collection.insert(n, onExistence = sc::reportInsert, onDelete = sc::reportDelete);
    }

    public boolean contains(int key) {
	Node n = this.collection.searchWithoutRemoval(key);
	if (n == null)
	    return false;
	if (n.isMarked()) {
	    getSC().reportDelete(n);
	    return false;
	} else {
	    getSC().reportInsert(n);
	    return true;
	}
    }

    public Collection<?> takeSnapshot() {
	return acquireSnapCollector().collectSnapshot(this.collection.head());
    }

    private ICollector acquireSnapCollector() {
	ICollector sc = getSC();
	if (!sc.isActive()) {
	    this.psc.compareAndSet(sc, ICollector.create());
	    sc = getSC();
	}
	return sc;
    }

    private final AtomicReference<ICollector> psc = new AtomicReference<>(ICollector.DEACTIVATED);

    private ICollector getSC() {
	return this.psc.get();
    }
}
