package basic;

import java.util.function.Consumer;

public interface ILockFreeOrderedSet {
    /**
     * @param node
     *            to add
     * @param onExistence
     *            callback. Called immediately after verifying that the node
     *            with node.key exists in the collection, either because it was
     *            already there, or because we just put it there.
     * @param onDelete
     *            callback. The insert operation iterates over the collection
     *            looking for the right place to add the node. During this
     *            iteration, it may find a node that is marked but has not yet
     *            physicall removed. In such a case, it reports the node's
     *            removal using {@code onDelete} callback.
     * @return {@code true} if the node did not exist, {@code false} if it did
     */
    boolean insert(Node node, Callback onExistence, Callback onDelete);

    /**
     * @param key
     *            to delete
     * @param onBetweenMarkAndRemove
     *            callback. Called during the deletion process, after marking
     *            the node for removal and before physically removing it.
     * @param onDelete
     *            callback. The insert operation iterates over the collection
     *            looking for the right place to add the node. During this
     *            iteration, it may find a node that is marked but has not yet
     *            physicall removed. In such a case, it reports the node's
     *            removal using {@code onDelete} callback.
     * @return whether a node was removed
     */
    boolean delete(int key, Callback onBetweenMarkAndRemove, Callback onDelete);

    /**
     * We "search without removal" since {@code contains(key)} queries should be
     * as fast as possible. Hence, no callbacks are passed here; the only
     * interesting operation will be done on the returning node, if any.
     * 
     * @param key
     *            to find
     * @return a node s.t. {@code node.key() == key}
     */
    Node searchWithoutRemoval(int key);

    /**
     * @return node to start collecting from.
     */
    Node head();
}

/**
 * A pretty name for the methods that are passed as callback to
 * {@link ILockFreeOrderedSet}
 *
 */
interface Callback extends Consumer<Node> {

}

abstract class Utils {

    // fake, for syntactic helping
    static Callback onDelete, onExistence, onBetweenMarkAndRemove;

}