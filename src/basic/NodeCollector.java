package basic;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

final class NodeCollector {

    final AtomicReference<Node> snapTail = new AtomicReference<>(new Node() {
	AtomicReference<Node> _next = null;
	public boolean isMarked() { return false; }
	public int key() { return Integer.MIN_VALUE; }
	public AtomicReference<Node> next() { return this._next; }
    });

    Node addNodeWhyNot(Node node) {
	Node tail = snapTail.get();
	if (tail.key() < node.key()) {
	    boolean wasLast = tail.next().compareAndSet(null, node);
	    Node toSet = wasLast ? node : tail.next().get();
	    snapTail.compareAndSet(tail, toSet);
	}
	// (If it is correct, the method can be split in two)
	return snapTail.get();
    }

    /*
     * Analogue to copying a register into array C. Installs a pointer to the
     * given node. May fail to install the pointer if the BlockFurtherPointers()
     * method has previously been invoked.
     * 
     * The basic idea in the implementation of AddNode is to use the lock-free
     * queue of Michael and Scott. To install a pointer to a node, a thread
     * reads the tail pointer. If the tail node is last, it attempts to add its
     * node after the last node and then fix the tail to point to the newly
     * added node. If the tail node is not the last node, i.e., its next field
     * hold to a non-null node, then the thread tries by a CAS to change the
     * tail to point to the next node, and retries adding its node again.
     * 
     * Clearly, this implementation is not wait-free as the thread may
     * repeatedly fail to add its node and make progress. We therefore use a
     * simple optimization that slightly alters the semantics of the AddNode
     * method. To this end, we note that nodes should be added to the snapshot
     * view in an ascending order of keys. The AddNode method will
     * (intentionally) fail to add any node whose key is smaller than or equal
     * to the key of the last node added to the snap-collector. When such a
     * failure happens, AddNode returns a pointer to the data structure node
     * that was last added to the snap-collector view of the snapshot. This way,
     * an iterating thread that joins in after a lot of pointers have already
     * been installed, simply jumps to the current location. This also reduces
     * the number of pointers in the snap-collector object to reflect only the
     * view of a single sequential traverse, avoiding unnecessary duplications.
     * But most importantly, it allows wait-freedom.
     * 
     * The snap-collector object still holds a tail pointer to the queue (which
     * might at times point to the node before last). To enqueue a pointer to a
     * node that holds the key k, a thread reads the tail pointer. If the tail
     * node holds a key greater than or equal to k, it doesn’t add the node and
     * simply returns the tail node. If the tail node is not the last node,
     * i.e., its next field hold to a non-null node, then this means that there
     * is another thread that has just inserted a new node to the snapshot view.
     * In this case, this new node is either the same node we are trying to add
     * or a larger one. So in this case the thread tries by a CAS to change the
     * tail to point to the next node (similarly to [12]), and then it returns
     * the new tail, again without adding the new node.
     * 
     * This optimization serves three purposes: 1. It allows new iterating
     * threads to jumps to the current location; 2. It makes the AddNode method
     * fast and wait-free; and 3. It keeps the list of pointers to nodes sorted
     * by their keys, which then allows a simple iteration over the keys in the
     * snapshot.
     */
    /**
     * @param node
     *            new node to add
     */
    // this is dangerous, Shahar... it is hard to be sure this works correctly.
    Node addNode(Node node) {
	Node last = snapTail.get();
	// trying to add an out of place node.
	if (last.key() >= node.key())
	    return last;

	if (last.next().get() != null) {
	    if (last == snapTail.get())
		snapTail.compareAndSet(last, last.next().get());
	    // Not putting the suggested node, as another one was put instead of
	    // it.
	    return snapTail.get();
	} else if (last.next().compareAndSet(null, node)) {
	    snapTail.compareAndSet(last, node);
	    return node;
	} else {
	    return snapTail.get();
	}
    }

    Node addNode1(Node node) {
	// To enqueue a pointer to a node, a thread reads the tail pointer.
	Node tail = snapTail.get();

	/*
	 * If the tail node holds a key greater than or equal to k, it doesn’t
	 * add the node and simply returns the tail node.
	 */
	if (tail.key() >= node.key())
	    return tail;

	if (tail.next().get() == null) {
	    // If the tail node is last, it attempts to add its node after the
	    // last node...
	    if (tail.next().compareAndSet(null, node)) {
		// ... and then fix the tail to point to the newly added node.
		snapTail.compareAndSet(tail, node);
		return node;
	    }
	} else {
	    /*
	     * If the tail node is not the last node, i.e., its next field hold
	     * to a non-null node, then this means that there is another thread
	     * that has just inserted a new node to the snapshot view.
	     * 
	     * In this case, this new node is either the same node we are trying
	     * to add or a larger one. So in this case the thread tries by a CAS
	     * to change the tail to point to the next node, ...
	     */
	    if (tail == snapTail.get())
		snapTail.compareAndSet(tail, tail.next().get());
	}
	// ... and then it returns the new tail, again without adding the new
	// node.
	return snapTail.get();
    }

    /**
     * Required to synchronize between multiple iterators.
     * <p>
     * After this method is completed, any further calls to {@code addNode()}
     * will do nothing.<br>
     * Concurrent calls to {@code addNode()} may succeed or fail arbitrarily.
     */
    void blockFurtherNodes() {
	snapTail.set(Node.BLOCKER);
    }

    /**
     * @return and Identity-based set
     */
    Set<Node> getAllNodes() {
	Set<Node> res = Collections.newSetFromMap(new IdentityHashMap<Node, Boolean>());
	for (Node curr = snapTail.get(); curr != Node.BLOCKER; curr = curr
		.next().get()) {
	    res.add(curr);
	}
	return res;
    }
}
