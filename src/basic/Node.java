package basic;

import java.util.concurrent.atomic.AtomicReference;

public interface Node {
    boolean isMarked();
    int key();
    AtomicReference<Node> next();
    
    
    Node BLOCKER = new Node() {
	final AtomicReference<Node> self = new AtomicReference<>(BLOCKER);
	@Override
	public AtomicReference<Node> next() {
	    return self;
	}

	@Override
	public int key() {
	    return Integer.MAX_VALUE;
	}

	@Override
	public boolean isMarked() {
	    return false;
	}
    };
}
