package erez;

//import java.io.FileNotFoundException;
//import java.io.ObjectInputStream.GetField;
//import java.text.DecimalFormat;
//import java.util.concurrent.atomic.AtomicInteger;


//import impl.*;

public class Test {
	//private static final int NUM_THREADS = 1;
	//private static final int NUM_ITERS = 10000;
	//private static final int QUEUE_TYPE = 0;
	//private static final int TEST_TYPE = 0;

	public static int numThreads = 32;
//	
//	static int Max_Failures = 2;
//	static int repeat = 16;
//	public static int[][] Actions;
//	enum ListType {List, ListCombined, FRList, FRCombined, BST, BSTCombined, Skip, SkipCombined, LeaSkip, LF, WF, FPSP, LFV1,LFV2, EllenUC, WFOPHelp, SkipIT, 
//		        LeaSkipIT, CTrieIT, Trevor, DCFineList, DCList, FineList, LazyCoarseList, LazyList, LazyListOpt, LockFreeList, OptimisticList};
//	enum QueueType {MS, Dif}
//	//public static int numIters = NUM_ITERS;
//	//private static int queueType = QUEUE_TYPE;
//	//private static int testType = TEST_TYPE;
//
//	public static void Run(String[] args) throws InterruptedException, FileNotFoundException {
//		Actions = new int[numThreads][2000000];
//		int repeatExp = repeat;
//		int actionsPerThreads = 100000;
//		//IteratorTest();
//		//if (4 > 3)
//		//	return;
//		/*for (int j = 0; j < repeatExp; j++) {
//		 // RunTestQueue(QueueType.MS, 1, 500000);
//		 // RunTestQueue(QueueType.Dif, 1, 500000);
//		 // RunTestQueueDif(QueueType.Dif, 8, 500000);
//		 // RunTestQueue(QueueType.Dif, 8, 500000);
//		 // RunTestQueue(QueueType.MS, 8, 500000);
//		  RunTestLists(ListType.LF, 0, 1, 100000, 32);
//		  RunTestLists(ListType.LFV1, 0, 1, 100000, 32);
//		  RunTestLists(ListType.LFV2, 0, 1, 100000, 32);
//		  RunTestLists(ListType.LF, 0, 1, 100000, 1024);
//		  RunTestLists(ListType.LFV1, 0, 1, 100000, 1024);
//		  RunTestLists(ListType.LFV2, 0, 1, 100000, 1024);
//		  RunTestLists(ListType.LF, 0, 8, 100000, 32);
//		  RunTestLists(ListType.LFV1, 0, 8, 100000, 32);
//		  RunTestLists(ListType.LFV2, 0, 8, 100000, 32);
//		  RunTestLists(ListType.LF, 0, 8, 100000, 1024);
//		  RunTestLists(ListType.LFV1, 0, 8, 100000, 1024);
//		  RunTestLists(ListType.LFV2, 0, 8, 100000, 1024);
//		  RunTestLists(ListType.LF, 0, 16, 100000, 32);
//		  RunTestLists(ListType.LFV1, 0, 16, 100000, 32);
//		  RunTestLists(ListType.LFV2, 0, 16, 100000, 32);
//		  RunTestLists(ListType.LF, 0, 16, 100000, 1024);
//		  RunTestLists(ListType.LFV1, 0, 16, 100000, 1024);
//		  RunTestLists(ListType.LFV2, 0, 16, 100000, 1024);
//
//
//	  }*/ // Combined Test:
//		/*
//	 for (int j = 0; j < repeatExp; j++) {
//		  System.out.println("J is :" + j);
//		  for (int range=64; range <= 1024; range *= 16) {
//			  for (int threads = 1; threads <= numThreads; threads++) {
//				  RunCombinedTestNC(ListType.List, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.ListCombined, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.Skip, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.SkipCombined, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.BST, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.BSTCombined, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.FRList, 0, threads, actionsPerThreads, range);
//				  RunCombinedTestNC(ListType.FRCombined, 0, threads, actionsPerThreads, range);
//			  }
//		  }
//	  }
//	  /// LIST COMPARISON DISC 2014 TM T ///
//	   */
//	  for (int j = 0; j < repeatExp; j++) {
//		  System.out.println("J is :" + j);
//		  int contains = 50;
//		  double cChance = (double)contains/100.0;
//		  for (int range = 32; range <=1024; range*=32) {
//			  for (int i = 1; i <= numThreads; i++) {
//				  RunTestNCBoundedTime(ListType.LF, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.FRList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.DCFineList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.DCList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.FineList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.LazyCoarseList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.LazyList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.LazyListOpt, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.LockFreeList, 0, i, 100000, range, cChance, 100, 100);
//				  RunTestNCBoundedTime(ListType.OptimisticList, 0, i, 100000, range, cChance, 100, 100);
//			  }
//		  }
//	  }
//	  
//	  
//	  /// END LIST COMPARIOSN DISC 2014 TM T///
//	   
//	  /*
//		int numA = 2;
//		for (int contains = 50; contains <= 60; contains+=20)
//		{
//			System.out.println("Contains is : " + contains);
//			double cChance = (double)contains/100.0;
//			for (int j = 0; j <12; j++) {
//				System.out.println("J is :" + j);
//
//				numA = 1000;
//
//				// iterator test.
//				for (int i = 1; i <= numThreads; i++) {
//					//RunITTestNCBoundedTime(ListType.CTrieIT, 00, i, 100000, 1024, cChance, 1000, 2000, 100);
//					//RunITTestNCBoundedTime(ListType.LeaSkipIT, 00, i, 100000, 1024, cChance, 1000, 2000, 100);
//					//RunITTestNCBoundedTime(ListType.SkipIT, 100000, i, 1000000, 10000000, cChance, 1000000000, 2000, 100);
//					//RunITTestNCBoundedTime(ListType.Trevor, 100000, i, 1000000, 10000000, cChance, 1000000000, 2000, 100);
//				}
//
//
//				for (int i = 1; i <= numThreads; i++) {
//					//RunTestNC(ListType.LF, 0, i, 100000,32,cChance);
//					//RunTestNC(ListType.WF, 0, i, 1000000,32,cChance);
//					//RunTestNC(ListType.FPSP, 0, i, 100000,32,cChance);
//					//RunSillyTest(i);
//					//RunTestNCBoundedTime(ListType.LF, 0, i, 100000,32,cChance, 2000, 100);
//					//RunTestNCBoundedTime(ListType.WF, 0, i, 100000,32,cChance, 2000, 80000);
//					//RunTestNCBoundedTime(ListType.FPSP, 0, i, 100000,32,cChance, 2000, 100);
//					//RunTestNCBoundedTime(ListType.Skip, 0, i, 100000,32,cChance, 2000, 100);
//					//RunTestNCBoundedTime(ListType.LeaSkip, 0, i, 100000,32,cChance, 2000, 100);
//				}
//				numA = 10000;
//				for (int i = 1; i <= numThreads; i++) {
//					//RunTestNCBoundedTime(ListType.LF, 0, i, 100000,1024,cChance,2000,100);
//					RunTestNCBoundedTime(ListType.WF, 0, i, 100000,1024,cChance,2000,2500);
//					RunTestNCBoundedTime(ListType.FPSP, 0, i, 100000,1024,cChance,2000,100);
//					//RunTestNCBoundedTime(ListType.WFOPHelp, 0, i, 100000,102400,cChance,5000,100);
//					//RunTestNCBoundedTime(ListType.Skip, 0, i, 100000,1024,cChance,2000,100);
//					//RunTestNCBoundedTime(ListType.LeaSkip, 0, i, 100000,1024,cChance,2000,100);
//				}
//				numA = 10000;
//				for (int i = 1; i < 33; i++) {
//					//  RunTest(ListType.LF, numA, i, numA/i);
//					//  RunTest(ListType.WF, numA, i, numA/i);
//					//  RunTest(ListType.FPSP, numA, i, numA/i);
//				}
//				numA = 10000;
//				for (int i = 1; i < 33; i++) {
//					//  RunTest(ListType.LF, numA, i, numA/i,100000);
//					//  RunTest(ListType.WF, numA, i, numA/i,100000);
//					//  RunTest(ListType.FPSP, numA, i, numA/i,100000);
//				}
//				numA = 10000;
//				for (int i = 1; i < 33; i++) {
//					//  RunTest(ListType.LF, numA, i, numA/i);
//					//  RunTest(ListType.WF, numA, i, numA/i);
//					//  RunTest(ListType.FPSP, numA, i, numA/i);
//				}
//				numA = 10000;
//				for (int i = 1; i < 33; i++) {
//					//  RunTest(ListType.LF, numA, i, 10000,100000);
//					//  RunTest(ListType.WF, numA, i, 10000,100000);
//					//  RunTest(ListType.FPSP, numA, i, 10000,100000);
//				}
//			}
//		}
//		/* for (int j = 0; j <20; j++) {
//		 System.out.println("J is :" + j);
//	  numA = 32768;
//	  for (int i = 1; i < 33; i++) {
//		  RunTest(ListType.LF, numA, i, numA/i);
//		  RunTest(ListType.WF, numA, i, numA/i);
//		  RunTest(ListType.FPSP, numA, i, numA/i);
//	  }
//	  numA = 65536;
//	  for (int i = 1; i < 33; i++) {
//		  RunTest(ListType.LF, numA, i, numA/i);
//		  RunTest(ListType.WF, numA, i, numA/i);
//		  RunTest(ListType.FPSP, numA, i, numA/i);
//	  }
//	  numA = 32768;
//	  for (int i = 1; i < 33; i++) {
//		  RunTest(ListType.LF, numA, i, numA/i,100000);
//		  RunTest(ListType.WF, numA, i, numA/i,100000);
//		  RunTest(ListType.FPSP, numA, i, numA/i,100000);
//	  }
//	  numA = 65536;
//	  for (int i = 1; i < 33; i++) {
//		  RunTest(ListType.LF, numA, i, numA/i,100000);
//		  RunTest(ListType.WF, numA, i, numA/i,100000);
//		  RunTest(ListType.FPSP, numA, i, numA/i,100000);
//	  }
//	 }*/
//		/*
//	  long estimatedTime = 0;
//	  for (int j = 0; j < 100; j++)
//	  {
//	  IList list = new SkipList();
//	  //list.insert(0, 8);
//	  //list.insert(0, 18);
//	  //list.insert(0, 77);
//	  Thread[] threads = new Thread[numThreads];
//
//	  //for (int q = 1; q <= 10000; q++)
//	//	  list.insert(0, q);
//	 // list.insert(0, 4);
//	  //list.insert(0, 5);
//	 // list.insert(0, 6);
//	  //threads[0] = new TestThread(list, 0, 1, 1, 4000, 1, 1, 4000);
//	  //threads[1] = new TestThread(list, 1, 7, 7, 4000, 7, 7, 4000);
//	 // Correctness test :
//		long totalTime = 0;
//		for (int j = 0; j < 10; j++)
//		{
//			IList list =  new DCList();
//			System.out.println("hi");
//			//list.insert(0, 100000);
//			//list.insert(0, 100001);
//			Thread[] threads = new Thread[numThreads];
//			threads[0] = new TestThread(list,0,1,1,50,1,1,50);
//			threads[1] = new TestThread(list,1,1,300,50,1,300,50);
//			threads[2] = new TestThread(list,2,1,300,50,1,300,50);
//			threads[3] = new TestThread(list,3,1,300,80,1,300,80);
//			threads[4] = new TestThread(list,4,1,300,80,1,300,80);
//			threads[5] = new TestThread(list,5,1,3,200,1,3,200);
//			threads[6] = new TestThread(list,6,1,4,150,1,4,150);
//			threads[7] = new TestThread(list,7,1,4,150,1,4,150);
//			//threads[8] = new TestThread(list,8,2,2,4000,2,2,4000);
//			//threads[9] = new TestThread(list,9,2,2,4000,2,2,4000);
//			long startTime = System.currentTimeMillis();
//			for (int i = 0; i < threads.length; i++) {
//				threads[i].start();
//			}
//
//			for (int i = 0; i < threads.length; i++) {
//				threads[i].join();
//			}
//			totalTime += System.currentTimeMillis() - startTime;
//			//estimatedTime += System.currentTimeMillis() - startTime;
//			// ((WFList5)list).JustHelp();
//			//if (list.delete(0, 1))
//		//		System.out.println("Blat!!!1");
//		//	if (list.delete(0, 2))
//		//		System.out.println("Blat!!!2");
//		//	if (list.delete(0, 3))
//		//		System.out.println("Blat!!!3");
//			//list.delete(0, 100000);
//			//list.delete(0, 8);
//			//list.delete(0, 18);
//			//list.delete(0, 77);
//			if (list.IsEmpty()) {
//				System.out.println("List is Empty " + j);
//			}
//			else
//			{
//				System.out.println("List is not Empty");
//				list.PrintList();
//			}
//
//
//			//System.out.println();
//		}
//		System.out.println(totalTime);
//
//
//		/* List list = new WFList5();
//	  Thread[] threads = new Thread[numThreads];
//	  long estimatedTime = 0;
//	 for (int j = 0; j < 100; j++)
//	 {
//		 System.out.println(j);
//		 threads[0] = new EnqueDequeThread(list,0,70001,100000);
//		 threads[1] = new EnqueDequeThread(list,1, 170001, 200000);
//		 threads[2] = new EnqueDequeThread(list,2, 270001, 300000);
//		 threads[3] = new EnqueDequeThread(list,3, 370001, 400000);
//		 threads[4] = new EnqueDequeThread(list,4, 470001, 500000);
//		 threads[5] = new EnqueDequeThread(list,5, 570001, 600000);
//		 threads[6] = new EnqueDequeThread(list,6, 670001, 700000);
//		 threads[7] = new EnqueDequeThread(list,7, 770001, 800000);
//
//		  long startTime = System.currentTimeMillis();
//
//	    for (int i = 0; i < threads.length; i++) {
//	      threads[i].start();
//		}
//
//	    for (int i = 0; i < threads.length; i++) {
//	      threads[i].join();
//	    }
//
//	    estimatedTime += System.currentTimeMillis() - startTime;
//	 }
//    System.out.println("Time : " + estimatedTime);
//   /* if (list.IsEmpty())
//    	System.out.println("List is Empty");
//    else
//    {
//    	System.out.println("List is not Empty");
//    	list.PrintList();
//    }*/
//
//
//		//System.out.println();		
//	}
//
//	private static void RunTest(ListType t, int preInsertions, int numOfThreads, int actionsPerThread) {
//		CList list;
//		if (t == ListType.WF)
//			list = new FRList();
//		else if (t == ListType.LF)
//			list = new LFList();
//		else //(t== ListType.FPSP)
//			list = new FPSPOList();
//		PredefinedTestThread.FillList(list, preInsertions);
//		PredefinedTestThread[] threads = new PredefinedTestThread[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread.get(actionsPerThread, i, list,Actions[i]);
//		long startTime = System.currentTimeMillis();
//
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	private static void RunTest(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range) {
//		CList list;
//		if (t == ListType.WF)
//			list = new FRList();
//		else if (t == ListType.LF)
//			list = new LFList();
//		else //(t== ListType.FPSP)
//			list = new CombinedList();
//		PredefinedTestThread.FillList(list, preInsertions,range);
//		PredefinedTestThread[] threads = new PredefinedTestThread[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread.get(actionsPerThread, i, list, Actions[i],range);
//		long startTime = System.currentTimeMillis();
//
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		System.out.println(t + " pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	private static void RunSillyTest(int numOfThreads) {
//		AtomicInteger[] pr = new AtomicInteger[numOfThreads];
//		TestThreadSilly[] threads = new TestThreadSilly[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++) {
//			pr[i] = new AtomicInteger();
//			threads[i] = new TestThreadSilly(pr[i]);
//		}
//		System.gc();
//		long startTime = System.currentTimeMillis();
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		System.gc();
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		System.out.println("Five Million CASes on private variables threads: " + numOfThreads + " time: " + estimatedTime);
//	}
//
//	private static void RunTestNC(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range, double cChance) {
//		CList list;
//		if (t == ListType.WF)
//			list = new WFList11O();
//		else if (t == ListType.LF)
//			list = new LFList();
//		else //(t== ListType.FPSP)
//			list = new FPSPOList();
//		//for (int k = range; k >= 0; k--)
//		//	list.insert(0, k);
//		PredefinedTestThread.FillList(list, preInsertions,range);
//		PredefinedTestThread[] threads = new PredefinedTestThread[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread.getNCWithContains(actionsPerThread, i, list, Actions[i],range, cChance);
//		System.gc();
//		long startTime = System.currentTimeMillis();
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		System.gc();
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		if (t == ListType.FPSP && 2 > 3)
//			System.out.println(t + " NC pre insertions: " + ((FPSPOList2)list).size() + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		else
//			System.out.println(t + " NC pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	private static void RunTestNCBoundedTime(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range, double cChance, int millis, int checkRate) {
//		CList list = null;
//		if (t == ListType.WF)
//			list = new FPSPOList2();
//		else if (t == ListType.LF)
//			list = new LFList();
//		else if (t == ListType.FRList)
//			list = new FRList();
//		else if (t== ListType.FPSP)
//			list =  new ellenUCList(); //new FPSPOList2();
//		else if (t== ListType.EllenUC)
//			list = new ellenUCList();
//		else if (t== ListType.WFOPHelp)
//			list = new WFList12O();
//		else if (t == ListType.Skip)
//			list = new SkipList();
//		else if (t == ListType.LeaSkip)
//			list = new LeaSkip2();
//		else if (t == ListType.DCFineList)
//			list = new DCFineList();
//		else if (t == ListType.DCList)
//			list = new DCList();
//		else if (t == ListType.FineList)
//			list = new FineList();
//		else if (t == ListType.LazyCoarseList)
//			list = new LazyCoarseList();
//		else if (t == ListType.LazyList)
//			list = new LazyList();
//		else if (t == ListType.LazyListOpt)
//			list = new LazyListOpt();
//		else if (t == ListType.LockFreeList)
//			list = new LockFreeList();
//		else if (t == ListType.OptimisticList)
//			list = new OptimisticList();
//		//for (int k = range; k >= 0; k--)
//		//	list.insert(0, k);
//		PredefinedTestThread2.FillList(list, preInsertions,range);
//		PredefinedTestThread2[] threads = new PredefinedTestThread2[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread2.getNCWithContains(actionsPerThread, i, list, Actions[i],range, cChance,millis, checkRate);
//		System.gc();
//		long startTime = System.currentTimeMillis();
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}	
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		System.gc();
//
//		// and now to one of the most ugly patches in human history:
//		int totalActions = 0;
//		for (int i = 0; i < threads.length; i++)
//			totalActions += Actions[i][0];
//		estimatedTime = totalActions;
//
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		if (t == ListType.FPSP && 2 > 3)
//			System.out.println(t + " NC pre insertions: " + ((FPSPOList2)list).size() + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		else
//			System.out.println(t + " NC pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	// for iterator measurements - also quering size.
//	private static void RunITTestNCBoundedTime(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range, double cChance, int sizeOnceEvery,  int millis, int checkRate) {
//		CList list = null;
//		if (t == ListType.SkipIT)
//			list = new SkipIT();
//		else if (t == ListType.LeaSkipIT)
//			list = new LeaIter();
//		else if (t== ListType.CTrieIT)
//			list =  new CTrieIT(); 
//		else if (t == ListType.Trevor)
//			list = new TRVWrapper();
//
//		//for (int k = range; k >= 0; k--)
//		//	list.insert(0, k);
//		PredefinedTestThread2.FillList(list, preInsertions,range);
//		PredefinedTestThread2[] threads = new PredefinedTestThread2[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread2.getNCWithSize(actionsPerThread, i, list, Actions[i],range, cChance, sizeOnceEvery ,millis, checkRate);
//		System.gc();
//		long startTime = System.currentTimeMillis();
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}	
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		System.gc();
//
//		// and now to one of the most ugly patches in human history:
//		int totalActions = 0;
//		for (int i = 0; i < threads.length; i++)
//			totalActions += Actions[i][0];
//		estimatedTime = totalActions;
//
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		if (t == ListType.FPSP && 2 > 3)
//			System.out.println(t + " NC pre insertions: " + ((FPSPOList2)list).size() + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		else
//			System.out.println(t + " NC pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	private static void RunTestLists(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range) {
//		CList list;
//		if (t == ListType.LF)
//			list = new LFList();
//		else if (t == ListType.LFV1)
//			list = new LFLL_V1();
//		else //(t== ListType.FPSP)
//			list = new LFLL_V2();;
//			PredefinedTestThread.FillList(list, preInsertions,range);
//			PredefinedTestThread[] threads = new PredefinedTestThread[numOfThreads];
//			for (int i = 0; i < numOfThreads; i++)
//				threads[i] = PredefinedTestThread.getNC(actionsPerThread, i, list, Actions[i],range);
//			long startTime = System.currentTimeMillis();
//
//			for (int i = 0; i < threads.length; i++) {
//				threads[i].start();
//			}
//
//			for (int i = 0; i < threads.length; i++) {
//				try {
//					threads[i].join();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					System.out.println("Blay !! Execption !!");
//				}
//			}
//
//			long estimatedTime = System.currentTimeMillis() - startTime;
//			//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//			System.out.println(t + " NC pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//
//	private static void RunCombinedTestNC(ListType t, int preInsertions, int numOfThreads, int actionsPerThread, int range) {
//		CList list;
//		if (t == ListType.List)
//			list = new LFList();
//		else if (t == ListType.ListCombined)
//			list = new CombinedList2(Max_Failures);
//		else if (t == ListType.BST)
//			list = new BSTree();
//		else if (t == ListType.BSTCombined)
//			list = new BSTCombined(Max_Failures);
//		else if (t == ListType.FRList)
//			list = new FRList();
//		else if (t == ListType.FRCombined)
//			list = new FRCombined3(Max_Failures);
//		else if (t == ListType.Skip)
//			list = new SkipList();
//		else if (t == ListType.SkipCombined)
//			list = new SkipCombined2(Max_Failures);
//		else if (t == ListType.LeaSkip)
//			list = new LeaSkip2();
//		else 
//			list = null;
//		PredefinedTestThread.FillList(list, preInsertions,range);
//		PredefinedTestThread[] threads = new PredefinedTestThread[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = PredefinedTestThread.getNCWithContains(actionsPerThread, i, list, Actions[i],range,0.5);
//		long startTime = System.currentTimeMillis();
//
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		if (list instanceof BaseCombiner)  {
//			int helpAsked = ((BaseCombiner)list).GetHelpCount();
//			System.out.println(t + " NC pre insertions: " + helpAsked + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		}
//		else
//			System.out.println(t + " NC pre insertions: " + preInsertions + " range: " + range + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//	}
//
//	private static void RunTestQueue(QueueType t, int numOfThreads, int numIters) {
//		IQueue q;
//		if (t == QueueType.MS)
//			q = new MSQueueInt();
//		else //(t== QueueType.Dif)
//			q = new QueueDif2();
//		//for (int i = 0; i < 25; i++)
//		//	  q.enqueue(1, 0);
//		TestThreadQueue[] threads = new TestThreadQueue[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = new TestThreadQueue(q, i, 0, numIters);
//		long startTime = System.currentTimeMillis();
//
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		if (q instanceof QueueDif3)
//			((QueueDif3)q).quit();
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		System.out.println(t +  " threads: " + numOfThreads + " actions: " + numIters+ " time: " + estimatedTime);
//	}
//
//	private static void RunTestQueueDif(QueueType t, int numOfThreads, int numIters) {
//		QueueDif4 q = new QueueDif4(); 
//
//		//for (int i = 0; i < 25; i++)
//		//	  q.enqueue(1, 0);
//		TestThreadQueueDif[] threads = new TestThreadQueueDif[numOfThreads];
//		for (int i = 0; i < numOfThreads; i++)
//			threads[i] = new TestThreadQueueDif(q, i, 0, numIters);
//		long startTime = System.currentTimeMillis();
//
//		for (int i = 0; i < threads.length; i++) {
//			threads[i].start();
//		}
//
//		for (int i = 0; i < threads.length; i++) {
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Blay !! Execption !!");
//			}
//		}
//
//		long estimatedTime = System.currentTimeMillis() - startTime;
//		q.quit();
//		//System.out.println(t + " pre insertions: " + preInsertions + " threads: " + numOfThreads + " actions: " + actionsPerThread + " time: " + estimatedTime);
//		System.out.println(t +  " Dif threads: " + numOfThreads + " actions: " + numIters+ " time: " + estimatedTime);
//	}
//
//
//	/* protected static void printHelp() {
//    System.out.println("Run command: java -jar queue.jar [thr] [iters] [queue] [test]");
//    System.out.println("  [thr]   is the number of threads, default: " + NUM_THREADS);
//    System.out.println("  [iters] is the number of iterations, default: " + NUM_ITERS);
//    System.out.println("  [queue] is the type of queue: 0 - BF, 1 - WF, 2 - Opt WF, default: " + QUEUE_TYPE);
//    System.out.println("  [test]  is the type of test: 0 - enq-deq pairs on empty queue, 1 - 50% enqs on queue with 1000 elements, default: " + TEST_TYPE);
//  }*/
//
//	/*protected static void setParams(String args[]) {	
//    if (args.length > 0) {
//      if (args[0].equals("help")) {			
//        printHelp();
//        System.exit(0);
//      }
//      numThreads = new Integer(args[0]); 
//    }
//
//    if (args.length > 1) {
//      numIters = new Integer(args[1]); 
//    }
//
//    if (args.length > 2) {
//      queueType = new Integer(args[2]);
//      if (queueType < 0 || queueType > 2) {
//        printHelp();
//        System.exit(-1);
//      }
//    }
//
//    if (args.length > 3) {
//      testType = new Integer(args[3]);
//      if (testType < 0 || testType > 5) {
//        printHelp();
//        System.exit(-1);
//      }
//    }
//  }*/
//
//	static void IteratorTest() throws InterruptedException {
//		for (int j = 0; j < 3; j++)
//		{
//			//LeaIter list = new LeaIter();
//			CTrieIT list = new CTrieIT();
//			//LFList list = new LFList();
//			for (int i = 299998; i >= 6; i--) {
//				list.insert(0, i);
//			}
//			list.delete(0, 500);
//			list.delete(0, 501);
//			list.delete(0, 999);
//			list.delete(0, 1000);
//
//			Thread[] threads = new Thread[numThreads];
//			threads[0] = new InsertDeleteAlterThread(list, 0, 500, 1000, 100000);
//			threads[1] = new InsertDeleteAlterThread(list, 1, 501, 999, 100000);
//			//threads[2] = new InsertDeleteAlterThread(list, 2, 1, 1001, 100000);
//			//threads[3] = new InsertDeleteAlterThread(list, 3, 2, 1002, 100000);
//			//threads[4] = new InsertDeleteAlterThread(list, 4, 1, 1000, 100000);
//			//threads[5] = new InsertDeleteAlterThread(list, 5, 1, 1000, 100000);
//			//threads[6] = new InsertDeleteAlterThread(list, 6, 1, 1000, 100000);
//			//threads[7] = new InsertDeleteAlterThread(list, 7, 1, 1000, 100000);
//
//			for (int i = 0; i < 2/*threads.length*/; i++) {
//				threads[i].start();
//			}
//
//			for (int i = 0; i < 10; i++) {
//				//int size = list.GetSnapshot(0).size();
//				//int size = list.size();
//
//				//if (size < 500 || size > 501) {
//				//	System.out.println(size);
//				//}
//
//				//list.GetSnapshot(0).PrintList();
//				System.out.println(list.size(2));
//				//list.PrintSnapShot(2);
//			}
//
//
//			for (int i = 0; i < 2/*threads.length*/; i++) {
//				threads[i].join();
//			}
//			System.out.println("DONE!!!!!!");
//		}
//	}
}
