package erez;

import java.util.concurrent.atomic.AtomicReference;

class SnapCollector<T> implements ISnapCollector<T> {
    ReportItem<T>[] reportHeads;
    ReportItem<T>[] reportTails;

    NodeWrapper<T> head;
    AtomicReference<NodeWrapper<T>> tail;
    ReportItem<T> blocker = new ReportItem<>(null, ReportType.ADD, -1);
    volatile boolean active;

    @SuppressWarnings("unchecked")
    public SnapCollector() {
        head = new NodeWrapper<T>(); // sentinel head.
        head.key = Integer.MIN_VALUE;
        tail = new AtomicReference<NodeWrapper<T>>(head);
        active = true;

        reportHeads = new ReportItem[Test.numThreads];
        reportTails = new ReportItem[Test.numThreads];
        for (int i = 0; i < Test.numThreads; i++) {
            // sentinel head.
            reportHeads[i] = new ReportItem<>(null, ReportType.ADD, -1);
            reportTails[i] = reportHeads[i];
        }
    }

    // implemented according to the optimization in A.3
    @Override
    public T addNode(T node, int key) {
        NodeWrapper<T> last = tail.get();
        if (last.key >= key) // trying to add an out of place node.
            return last.node;

        if (last.next.get() != null) {
            if (last == tail.get())
                tail.compareAndSet(last, last.next.get());
            return tail.get().node;
        }

        NodeWrapper<T> newNode = new NodeWrapper<>();
        newNode.node = node;
        newNode.key = key;
        if (last.next.compareAndSet(null, newNode)) {
            tail.compareAndSet(last, newNode);
            return node;
        } else {
            return tail.get().node;
        }
    }

    @Override
    public void report(int tid, T Node, ReportType t, int key) {
        ReportItem<T> tail = reportTails[tid];
        ReportItem<T> newItem = new ReportItem<>(Node, t, key);
        if (tail.next.compareAndSet(null, newItem))
            reportTails[tid] = newItem;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void blockFurtherNodes() {
        NodeWrapper<T> blocker = new NodeWrapper<>();
        blocker.node = null;
        blocker.key = Integer.MAX_VALUE;
        tail.set(blocker);
    }

    @Override
    public void deactivate() {
        this.active = false;
    }

    @Override
    public void blockFurtherReports() {
        for (int i = 0; i < Test.numThreads; i++) {
            ReportItem<T> tail = reportTails[i];
            if (tail.next.get() == null)
                tail.next.compareAndSet(null, blocker);
        }
    }

    @Override
    public NodeWrapper<T> readNodes() {
        return head;
    }

    @Override
    public ReportItem<T>[] readReports() {
        return reportHeads;
    }
}
