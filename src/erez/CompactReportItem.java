package erez;

class CompactReportItem<T> implements Comparable<CompactReportItem<T>> {
    T node;
    ReportType t;
    int key;
    int id;

    public CompactReportItem(T node, ReportType t, int key) {
	this.node = node;
	this.t = t;
	this.key = key;
    }

    public int compareTo(CompactReportItem<T> other) {
	if (this.key != other.key)
	    return this.key - other.key;
	// if (this.id != other.id)
	// return this.id - other.id;
	if (this.node != other.node)
	    return this.node.hashCode() - other.node.hashCode();
	return this.t.ordinal() - other.t.ordinal();
    }
}
