package erez;

import java.util.concurrent.atomic.*;

interface IList {
    public boolean insert(int tid, int key);

    public boolean delete(int tid, int key);

    public boolean contains(int tid, int key);

    public boolean IsEmpty();

    public int size();

    public void PrintList();
}

interface CList {
    public boolean insert(int tid, int key);

    public boolean delete(int tid, int key);

    public boolean contains(int tid, int key);

    public int size();
}

interface ITList {
    public boolean insert(int tid, int key);

    public boolean delete(int tid, int key);

    public boolean contains(int tid, int key);

    public int size(int tid);
}

public class LFList implements IList, CList, ITList {

    class Node {
	int key;
	AtomicMarkableReference<Node> next;

	public Node(int val) {
	    key = val;
	}
    }

    Node head;
    Node tail;

    public boolean IsEmpty() /* not thread safe */
    {
	return head.next.getReference() == tail;
    }

    public int size() /* not thread safe */
    {
	int result = 0;
	Node curr = head.next.getReference();
	while (curr != tail) {
	    result++;
	    curr = curr.next.getReference();
	}
	return result;
    }

    public void PrintList() /* not thread safe */
    {
	Node curr = head.next.getReference();
	while (curr != tail) {
	    System.out.print(curr.key + " ");
	    curr = curr.next.getReference();
	}
	System.out.println();
    }

    public LFList() {
	head = new Node(Integer.MIN_VALUE);
	tail = new Node(Integer.MAX_VALUE);
	head.next = new AtomicMarkableReference<Node>(tail, false);
	tail.next = new AtomicMarkableReference<Node>(tail, false);
    }

    class Window {
	public Node pred, curr;

	Window(Node myPred, Node myCurr) {
	    pred = myPred;
	    curr = myCurr;
	}
    }

    public Window find(Node head, int key) {
	Node pred = null, curr = null, succ = null;
	boolean[] marked = { false };
	boolean snip;
	retry: while (true) {
	    pred = head;
	    curr = pred.next.getReference();
	    while (true) {
		succ = curr.next.get(marked);
		while (marked[0]) {
		    snip = pred.next.compareAndSet(curr, succ, false, false);
		    if (!snip)
			continue retry;
		    curr = succ;
		    succ = curr.next.get(marked);
		}
		if (curr.key >= key) {
		    return new Window(pred, curr);
		}
		pred = curr;
		curr = succ;
	    }
	}
    }

    public boolean insert(int tid, int key) {
	while (true) {
	    Window window = find(head, key);
	    Node pred = window.pred, curr = window.curr;
	    if (curr.key == key) {
		return false;
	    } else {
		Node node = new Node(key);
		node.next = new AtomicMarkableReference<Node>(curr, false);
		if (pred.next.compareAndSet(curr, node, false, false))
		    return true;
	    }
	}
    }

    public boolean delete(int tid, int key) {
	boolean snip;
	while (true) {
	    Window window = find(head, key);
	    Node pred = window.pred, curr = window.curr;
	    if (curr.key != key) {
		return false;
	    } else {
		Node succ = curr.next.getReference();
		snip = curr.next.compareAndSet(succ, succ, false, true);
		if (!snip)
		    continue;
		pred.next.compareAndSet(curr, succ, false, false);
		return true;
	    }
	}
    }

    public boolean contains(int tid, int key) {
	boolean[] marked = { false };
	Node curr = head;
	while (curr.key < key) { 
	    // search for the key
	    curr = curr.next.getReference();
	    curr.next.get(marked);
	}
	// the key is found and is logically in the list.
	return (curr.key == key && !marked[0]); 
    }

    @Override
    public int size(int tid) {
	System.out.println("Size in LFList is not Implemented!");
	return 0;
    }
}
