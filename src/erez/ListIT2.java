package erez;

import java.util.concurrent.atomic.*;

/*
 * The first implementation of "Do then report" iterator.
 */
public class ListIT2 implements IList, CList, ITList {

    class Node {
	int key;
	AtomicMarkableReference<Node> next;

	public Node(int val) {
	    key = val;
	}
    }

    Node head;
    Node tail;
    AtomicReference<IteratorSaver<Node>> snapPointer;

    public ListIT2() {

	head = new Node(Integer.MIN_VALUE);
	tail = new Node(Integer.MAX_VALUE);
	head.next = new AtomicMarkableReference<Node>(tail, false);
	tail.next = new AtomicMarkableReference<Node>(tail, false);

	IteratorSaver<Node> dummy = new IteratorSaver<Node>();
	dummy.blockFurtherReports();
	snapPointer = new AtomicReference<IteratorSaver<Node>>(dummy);

    }

    class Window {
	public Node pred, curr;

	Window(Node myPred, Node myCurr) {
	    pred = myPred;
	    curr = myCurr;
	}
    }

    // can optimize here life LFLL_V1.
    public Window find(Node head, int key, int tid) {
	Node pred = null, curr = null, succ = null;
	boolean[] marked = { false };
	boolean snip;
	retry: while (true) {
	    pred = head;
	    curr = pred.next.getReference();
	    while (true) {
		succ = curr.next.get(marked);
		while (marked[0]) {
		    IteratorSaver<Node> sc = snapPointer.get();
		    if (sc.isActive()) {
			sc.report(tid, curr, ReportType.REMOVE, curr.key);
		    }
		    snip = pred.next.compareAndSet(curr, succ, false, false);
		    if (!snip)
			continue retry;
		    curr = succ;
		    succ = curr.next.get(marked);
		}
		if (curr.key >= key) {
		    return new Window(pred, curr);
		}
		pred = curr;
		curr = succ;
	    }
	}
    }

    public boolean insert(int tid, int key) {
	while (true) {
	    Window window = find(head, key, tid);
	    Node pred = window.pred, curr = window.curr;
	    if (curr.key == key) {
		IteratorSaver<Node> sc = snapPointer.get();
		if (sc.isActive()) {
		    if (!curr.next.isMarked())
			sc.report(tid, curr, ReportType.ADD, curr.key);
		}
		return false;
	    } else {
		Node node = new Node(key);
		node.next = new AtomicMarkableReference<Node>(curr, false);
		if (pred.next.compareAndSet(curr, node, false, false)) {
		    IteratorSaver<Node> sc = snapPointer.get();
		    if (sc.isActive()) {
			if (!node.next.isMarked())
			    sc.report(tid, node, ReportType.ADD, node.key);
		    }
		    return true;
		}
	    }
	}
    }

    public boolean delete(int tid, int key) {
	boolean snip;
	while (true) {
	    Window window = find(head, key, tid);
	    Node pred = window.pred, curr = window.curr;
	    if (curr.key != key) {
		return false;
	    } else {
		Node succ = curr.next.getReference();
		snip = curr.next.compareAndSet(succ, succ, false, true);
		if (!snip)
		    continue;
		IteratorSaver<Node> sc = snapPointer.get();
		if (sc.isActive()) {
		    sc.report(tid, curr, ReportType.REMOVE, curr.key);
		}
		if (!pred.next.compareAndSet(curr, succ, false, false))
		    find(head, key, tid);
		return true;
	    }
	}
    }

    public boolean contains(int tid, int key) {
	boolean[] marked = { false };
	Node curr = head;
	while (curr.key < key) { // search for the key
	    curr = curr.next.getReference();
	    curr.next.get(marked);
	}
	if (curr.key != key)
	    return false;
	IteratorSaver<Node> sc = snapPointer.get();
	if (sc.isActive()) {
	    if (curr.next.isMarked())
		sc.report(tid, curr, ReportType.REMOVE, curr.key);
	    else
		sc.report(tid, curr, ReportType.ADD, curr.key);
	}
	return (curr.key == key && !marked[0]); // the key is found and is
						// logically in the list.
    }

    public IteratorSaver<Node> GetSnapshot(int tid) {
	IteratorSaver<Node> result = null;
	result = snapPointer.get();
	if (!result.isActive()) {
	    IteratorSaver<Node> candidate = new IteratorSaver<Node>();
	    if (snapPointer.compareAndSet(result, candidate))
		result = candidate;
	    else
		result = snapPointer.get();
	}

	Node curr = head;
	while (curr != null) { // search for the key
	    curr = curr.next.getReference();
	    if (curr.key == Integer.MAX_VALUE) {
		result.blockFurtherNodes();
		break;
	    }
	    if (!curr.next.isMarked())
		curr = result.addNode(curr, curr.key);
	}

	result.blockFurtherReports();
	result.prepare(tid);
	return result;
    }

    public boolean IsEmpty() /* not thread safe */
    {
	return head.next.getReference() == tail;
    }

    public int size() /* not thread safe */
    {
	int result = 0;
	Node curr = head.next.getReference();
	while (curr != tail) {
	    result++;
	    curr = curr.next.getReference();
	}
	return result;
    }

    public void PrintList() /* not thread safe */
    {
	Node curr = head.next.getReference();
	while (curr != tail) {
	    System.out.print(curr.key + " ");
	    curr = curr.next.getReference();
	}
	System.out.println();
    }

    public void PrintSnapShot(int tid) {
	IteratorSaver<Node> snap = GetSnapshot(tid);
	Node curr;
	while ((curr = snap.getNext(tid)) != null) {
	    if (curr.key == 500 || curr.key == 501 || curr.key == 999
		    || curr.key == 1000 || curr.key < 4)
		System.out.print(curr.key + ", ");
	}
	System.out.println();
    }

    @Override
    public int size(int tid) {
	IteratorSaver<Node> snap = GetSnapshot(tid);
	int result = 0;
	while (snap.getNext(tid) != null) {
	    result++;
	}
	return result;
    }

}
