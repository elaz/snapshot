package erez;

public interface ISnapCollector<T> {
    /**
     * Analogue to copying a register into array C.
     * Installs a pointer to the given node.
     * May fail to install the pointer if the BlockFurtherPointers() method has previously been invoked.
     * @param node
     * @param key
     * @return
     */
    T addNode(T node, int key);
    
    /**
     * Analogue to reporting a new value in array B.
     * Installs the given report.
     * May fail to install the report if the BlockFurtherReports() method has previously been invoked.
     * @param tid
     * @param Node
     * @param t
     * @param key
     */
    void report(int tid, T Node, ReportType t, int key);
    
    /**
     * Analogue to reading the ongoingScan binary field. 
     * Returns true if the Deactivate() method has not yet been called, and false otherwise.
     * (True means the iteration is still ongoing and further pointers might still be installed in the snapshot object.)
     * @return
     */
    boolean isActive();
    /**
     * Analogue to setting ongoingScan to false.
     * After this method is complete, any call to IsActive returns false,
     * whereas before this method is invoked for the first time, IsActive returns true.
     */
    void deactivate();

    /**
     * Required to synchronize between multiple iterators.
     * After this method is completed, any further calls to AddNode will do nothing. 
     * Calls to AddNode concurrent with BlockFurtherPointers may fail or succeed arbitrarily.
     */
    void blockFurtherNodes();
    
    /**
     * Required to synchronize between multiple iterators.
     * After this method is completed, any further calls to Report will do nothing.
     * Calls to Report concurrent with BlockFurtherReports may succeed or fail arbitrarily.
     */
    void blockFurtherReports();
    
    NodeWrapper<T> readNodes();
    ReportItem<T>[] readReports();
}
