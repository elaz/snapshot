package erez;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import lea.LeaSourceWithIT2;


public class IteratorSaver<T> implements IIteratorSaver<T> {

    class NodeWrapper<V> {
	V node;
	AtomicReference<NodeWrapper<V>> next = new AtomicReference<>(null);
	int key; // this will hurt performance. Avoiding it would be so much nicer...
    }

    @SuppressWarnings("unchecked")
    // will hold NodeWrapper<T> of current locations of iterations.
    NodeWrapper<T>[] currLocations = new NodeWrapper[Test.numThreads];
    
    int[] currRepLocations = new int[Test.numThreads];
    ReportItem<T>[] reportHeads;
    ReportItem<T>[] reportTails;

    NodeWrapper<T> head;
    AtomicReference<NodeWrapper<T>> tail;
    ReportItem<T> blocker = new ReportItem<>(null, ReportType.ADD, -1);
    volatile boolean active;

    @SuppressWarnings("unchecked")
    public IteratorSaver() {
	head = new NodeWrapper<>(); // sentinel head.
	head.key = Integer.MIN_VALUE;
	tail = new AtomicReference<>(head);
	active = true;

	reportHeads = new ReportItem[Test.numThreads];
	reportTails = new ReportItem[Test.numThreads];
	for (int i = 0; i < Test.numThreads; i++) {
	    // sentinel head.
	    reportHeads[i] = new ReportItem<>(null, ReportType.ADD, -1); 
	    reportTails[i] = reportHeads[i];
	}

	gAllReports = new AtomicReference<>(null);
    }

    public IteratorSaver(boolean specialVariantOfLeaSourceWithIT3) {
	this();
	// this.specialVariantOfLeaSourceWithIT3 =
	// specialVariantOfLeaSourceWithIT3;
	if (specialVariantOfLeaSourceWithIT3)
	    System.out.println("ITeratorSaver: specialVariant not implemented");
    }

    @Override
    public void report(int tid, T Node, ReportType t, int key) {
	ReportItem<T> tail = reportTails[tid];
	ReportItem<T> newItem = new ReportItem<>(Node, t, key);
	if (tail.next.compareAndSet(null, newItem))
	    reportTails[tid] = newItem;
    }

    @Override
    public void blockFurtherReports() {
	this.active = false;
	for (int i = 0; i < Test.numThreads; i++) {
	    ReportItem<T> tail = reportTails[i];
	    if (tail.next.get() == null)
		tail.next.compareAndSet(null, blocker);
	    // If we failed to do this, all is fine as well.
	    // Either:
	    // 1. someone else has blocked
	    // 2. The thread has reported, and will not try to report again
	    // (since this IteratorSaver should be disconnected from the list
	    // prior to calling this method).
	}
    }

    void yieldOrSleep() {
	try {
	    Thread.sleep(20);
	} catch (InterruptedException e) {
	}
    }

    // this is dangerous, Shahar... it is hard to be sure this works correctly.
    @Override
    public T addNode(T node, int key) {
	NodeWrapper<T> last = tail.get();
	if (last.key >= key) // trying to add an out of place node.
	    return last.node;

	if (last.next.get() != null) {
	    // yieldOrSleep();
	    if (last == tail.get())
		tail.compareAndSet(last, last.next.get());
	    // Not putting the suggested node, as another one was put instead of it.
	    return tail.get().node;
	}

	NodeWrapper<T> newNode = new NodeWrapper<>();
	newNode.node = node;
	newNode.key = key;
	if (last.next.compareAndSet(null, newNode)) {
	    tail.compareAndSet(last, newNode);
	    return node;
	} else {
	    // yieldOrSleep();
	    return tail.get().node;
	}
    }

    @Override
    public void blockFurtherNodes() {
	NodeWrapper<T> blocker = new NodeWrapper<>();
	blocker.node = null;
	
	// to make sure it will block further nodes.
	blocker.key = Integer.MAX_VALUE;
	
	/*
	 * NodeWrapper<T> last = tail.get(); while
	 * (!last.next.compareAndSet(null, blocker)) { tail.compareAndSet(last,
	 * last.next.get()); last = tail.get(); }
	 */
	tail.set(blocker);

    }

    AtomicReference<ArrayList<CompactReportItem<T>>> gAllReports;

    @Override
    public void prepare(int tid) {
	currLocations[tid] = head;
	currRepLocations[tid] = 0;
	if (gAllReports.get() != null)
	    return;

	ArrayList<CompactReportItem<T>> allReports = new ArrayList<>();
	for (int i = 0; i < Test.numThreads; i++) {
	    AddReports(allReports, reportHeads[i]);
	    if (gAllReports.get() != null)
		return;
	}
	Collections.sort(allReports);
	// System.out.println("How many reports you ask?" + allReports.size());
	gAllReports.compareAndSet(null, allReports);
	return;
    }

    
    private void AddReports(ArrayList<CompactReportItem<T>> allReports,
	    ReportItem<T> curr) {
	curr = curr.next.get();
	while (curr != null && curr != blocker) {
	    allReports
		    .add(new CompactReportItem<T>(curr.node, curr.t, curr.key));
	    curr = curr.next.get();
	}
    }

    @Override
    public T getNext(int tid) {
	NodeWrapper<T> currLoc = currLocations[tid];
	int currRepLoc = currRepLocations[tid];
	ArrayList<CompactReportItem<T>> allReports = gAllReports.get();
	bigloop: while (true) {
	    CompactReportItem<T> rep = null;
	    int repKey = Integer.MAX_VALUE;
	    if (allReports.size() > currRepLoc) {
		rep = allReports.get(currRepLoc);
		repKey = rep.key;
	    }
	    int nodeKey = Integer.MAX_VALUE;
	    NodeWrapper<T> next = currLoc.next.get();
	    if (next != null)
		nodeKey = next.key;

	    // Option 1: node key < rep key. Return node.
	    if (nodeKey < repKey) {
		currLocations[tid] = next;
		currRepLocations[tid] = currRepLoc;
		return next.node;
	    }

	    // Option 2: node key == rep key
	    if (nodeKey == repKey) {
		// 2.a - both are infinity - iteration done.
		if (nodeKey == Integer.MAX_VALUE) {
		    currLocations[tid] = currLoc;
		    currRepLocations[tid] = currRepLoc;
		    return null;
		}
		// node and report with the same key ::

		// skip not-needed reports
		while (currRepLoc + 1 < allReports.size()) {
		    CompactReportItem<T> nextRep = allReports.get(currRepLoc + 1);
		    // dismiss a duplicate, or an insert followed by a matching
		    // delete:
		    if (rep.key == nextRep.key && rep.node == nextRep.node) {
			currRepLoc++;
			rep = nextRep;
		    } else
			break;
		}
		// standing on an insert report to a node I am holding:
		// 1. Return the current node.
		// 2. Skip over rest of reports for that key.
		if (rep.t == ReportType.ADD && rep.node == next.node) {
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = next;
		    return next.node;
		}
		// standing on an insert report to a different node than I hold:
		// 1. Return the reported node.
		// 2. Skip over rest of reports for that key.
		if (rep.t == ReportType.ADD && rep.node != next.node) {
		    T returnValue = rep.node;
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = next;
		    return returnValue;
		}
		// standing on a delete report to a different node than I hold:
		// skip over it and continue the big loop.
		if (rep.t == ReportType.REMOVE && rep.node != next.node) {
		    currRepLoc++;
		    continue bigloop;
		}
		// standing on a delete report to the node that I hold:
		// 1. advance over the node that I hold.
		// 2. advance with the report.
		// 3. continue the bigloop
		currLoc = next;
		currRepLoc++;
		continue;
	    }

	    // Option 3: node key > rep key
	    if (nodeKey > repKey) {
		// skip not-needed reports
		while (currRepLoc + 1 < allReports.size()) {
		    CompactReportItem<T> nextRep = allReports
			    .get(currRepLoc + 1);
		    // dismiss a duplicate, or an insert followed by a matching
		    // delete:
		    if (rep.key == nextRep.key && rep.node == nextRep.node) {
			currRepLoc++;
			rep = nextRep;
		    } else
			break;
		}
		// a delete report - skip over it.
		if (rep.t == ReportType.REMOVE) {
		    currRepLoc++;
		    continue bigloop;
		}

		// an insert report:
		// 1. skip over rest of the reports for the same key.
		// 2. return the node.
		if (rep.t == ReportType.ADD) {
		    T returnValue = rep.node;
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = currLoc;
		    return returnValue;
		}
	    }

	    System.out.println("Error in Iterator Saver Blat!");
	}
    }

    enum verOpts {
	StartRound, Returning, Dismissing, DeleteManage
    };

    public T GetNext(int tid, PrintStream varOut) {
	NodeWrapper<T> currLoc = currLocations[tid];
	int currRepLoc = currRepLocations[tid];
	ArrayList<CompactReportItem<T>> allReports = gAllReports.get();
	bigloop: while (true) {
	    CompactReportItem<T> rep = null;
	    int repKey = Integer.MAX_VALUE;
	    if (allReports.size() > currRepLoc) {
		rep = allReports.get(currRepLoc);
		repKey = rep.key;
	    }
	    int nodeKey = Integer.MAX_VALUE;
	    NodeWrapper<T> next = currLoc.next.get();
	    if (next != null)
		nodeKey = next.key;

	    Verbose(varOut, verOpts.StartRound, next, rep, nodeKey, repKey);
	    // Option 1: node key < rep key. Return node.
	    if (nodeKey < repKey) {
		currLocations[tid] = next;
		currRepLocations[tid] = currRepLoc;
		Verbose(varOut, verOpts.Returning, next.node);
		return next.node;
	    }

	    // Option 2: node key == rep key
	    if (nodeKey == repKey) {
		// 2.a - both are infinity - iteration done.
		if (nodeKey == Integer.MAX_VALUE) {
		    currLocations[tid] = currLoc;
		    currRepLocations[tid] = currRepLoc;
		    Verbose(varOut, verOpts.Returning);
		    return null;
		}
		// node and report with the same key ::

		// skip not-needed reports
		while (currRepLoc + 1 < allReports.size()) {
		    CompactReportItem<T> nextRep = allReports.get(currRepLoc + 1);
		    // dismiss a duplicate, or an insert followed by a matching
		    // delete:
		    if (rep.key == nextRep.key && rep.node == nextRep.node) {
			Verbose(varOut, verOpts.Dismissing, rep);
			currRepLoc++;
			rep = nextRep;
		    } else
			break;
		}
		// standing on an insert report to a node I am holding:
		// 1. Return the current node.
		// 2. Skip over rest of reports for that key.
		if (rep.t == ReportType.ADD && rep.node == next.node) {
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			Verbose(varOut, verOpts.Dismissing,
				allReports.get(currRepLoc));
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = next;
		    Verbose(varOut, verOpts.Returning, next.node);
		    return next.node;
		}
		// standing on an insert report to a different node than I hold:
		// 1. Return the reported node.
		// 2. Skip over rest of reports for that key.
		if (rep.t == ReportType.ADD && rep.node != next.node) {
		    T returnValue = rep.node;
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			Verbose(varOut, verOpts.Dismissing,
				allReports.get(currRepLoc));
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = next;
		    Verbose(varOut, verOpts.Returning, returnValue);
		    return returnValue;
		}
		// standing on a delete report to a different node than I hold:
		// skip over it and continue the big loop.
		if (rep.t == ReportType.REMOVE && rep.node != next.node) {
		    Verbose(varOut, verOpts.Dismissing, rep);
		    currRepLoc++;
		    continue bigloop;
		}
		// standing on a delete report to the node that I hold:
		// 1. advance over the node that I hold.
		// 2. advance with the report.
		// 3. continue the bigloop
		currLoc = next;
		currRepLoc++;
		Verbose(varOut, verOpts.DeleteManage, rep, next);
		continue;
	    }

	    // Option 3: node key > rep key
	    if (nodeKey > repKey) {
		// skip not-needed reports
		while (currRepLoc + 1 < allReports.size()) {
		    CompactReportItem<T> nextRep = allReports
			    .get(currRepLoc + 1);
		    // dismiss a duplicate, or an insert followed by a matching
		    // delete:
		    if (rep.key == nextRep.key && rep.node == nextRep.node) {
			Verbose(varOut, verOpts.Dismissing, rep);
			currRepLoc++;
			rep = nextRep;
		    } else
			break;
		}
		// a delete report - skip over it.
		if (rep.t == ReportType.REMOVE) {
		    Verbose(varOut, verOpts.Dismissing, rep);
		    currRepLoc++;
		    continue bigloop;
		}

		// an insert report:
		// 1. skip over rest of the reports for the same key.
		// 2. return the node.
		if (rep.t == ReportType.ADD) {
		    T returnValue = rep.node;
		    while (currRepLoc < allReports.size()
			    && allReports.get(currRepLoc).key == rep.key) {
			Verbose(varOut, verOpts.Dismissing,
				allReports.get(currRepLoc));
			currRepLoc++;
		    }
		    currRepLocations[tid] = currRepLoc;
		    currLocations[tid] = currLoc;
		    Verbose(varOut, verOpts.Returning, returnValue);
		    return returnValue;
		}
	    }

	    System.out.println("Error in Iterator Saver Blat!");
	}

    }

    @SuppressWarnings("unchecked")
    private void Verbose(PrintStream out, verOpts deletemanage,
	    CompactReportItem<T> rep, NodeWrapper<T> next) {
	String line = "Managing delete: ";
	if (next.node == null) {
	    line += " Node is Null ";
	} else {
	    // line += " Node ID: " + ((LeaSourceWithIT2.Node)next.node).count;
	    line += " Node Key: "
		    + ((LeaSourceWithIT2.Node<Integer, Boolean>) next.node).key;
	}
	if (rep == null) {
	    line += " Rep Is Null ";
	} else {
	    line += " Rep ID: " + rep.id;
	    line += " Rep Key: " + rep.key;
	    line += " Rep Order: " + rep.t;
	}
	out.println(line);

    }

    @SuppressWarnings("unchecked")
    private void Verbose(PrintStream out, verOpts returning, T node) {
	String line = "Returning node: ";
	if (node == null) {
	    line += " Node is Null ";
	} else {
	    // line += " Node ID: " + ((SkipIT.Node)node).count;
	    line += " Node Key: "
		    + ((LeaSourceWithIT2.Node<Integer, Boolean>) node).key;
	}
	out.println(line);

    }

    private void Verbose(PrintStream out, verOpts returning) {
	out.println("Returning null");
    }

    private void Verbose(PrintStream out, verOpts dismissing,
	    CompactReportItem<T> rep) {
	String line = "Dismissing Report: ";
	if (rep == null) {
	    line += " Rep Is Null ";
	} else {
	    line += " Rep ID: " + rep.id;
	    line += " Rep Key: " + rep.key;
	    line += " Rep Order: " + rep.t;
	}
	out.println(line);
    }

    private void Verbose(PrintStream out, verOpts startround,
	    NodeWrapper<T> next, CompactReportItem<T> rep, int nodeKey,
	    int repKey) {
	String line = "Starting Round ";
	if (next == null) {
	    line += " Wrapper is Null ";
	} else if (next.node == null) {
	    line += " Node is Null ";
	    line += nodeKey;
	} else {
	    // line += " Node ID: " + ((SkipIT.Node)next.node).count;
	    line += " Node Key: " + nodeKey;
	}
	if (rep == null) {
	    line += " Rep Is Null ";
	    line += repKey;
	} else {
	    line += " Rep ID: " + rep.id;
	    line += " Rep Key: " + repKey;
	    line += " Rep Order: " + rep.t;
	}
	out.println(line);
    }

    @Override
    public boolean isActive() {
	return active;
    }

    @Override
    public int getLastKey() {
	return tail.get().key;
    }
}
