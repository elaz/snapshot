package erez;

import java.util.concurrent.atomic.AtomicReference;


class ReportItem<T> {
	T node;
	ReportType t;
	AtomicReference<ReportItem<T>> next;
	int key;
	int id;

	public ReportItem(T node, ReportType t, int key) {
		this.node = node;
		this.t = t;
		this.next = new AtomicReference<ReportItem<T>>(null);
		this.key = key;
	}
}
