package erez;

public interface IIteratorSaver<T> {
    T addNode(T node, int key);
    void report(int tid, T Node, ReportType t, int key);

    boolean isActive();


    void blockFurtherNodes();
    void blockFurtherReports();
    
    void prepare(int tid);
    T getNext(int tid);
    int getLastKey();
}
